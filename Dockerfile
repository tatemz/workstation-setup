FROM ubuntu:18.04

LABEL maintainer="tate.barber@gmail.com" \
      description="A customized Docker image for local development within any environment."  \
      version="1.0.0"

# Install core dependencies
RUN apt-get update && apt-get -y install \
        bash \
        curl \
        git \
        passwd \
        sudo \
        vim \
        wget \
        zsh

ENV TERM xterm

# Setup user tatemz
ARG PASSWORD="tatemz"
RUN groupadd -r tatemz && \
    useradd -d /home/tatemz -ms /bin/zsh -g tatemz -G sudo tatemz && \
    echo "tatemz:${PASSWORD}" | chpasswd && \
    chage -d 0 tatemz && \
    mkdir /home/tatemz/Development && \
    chown -R tatemz:tatemz /home/tatemz/Development

# Add additional packages
RUN apt-get -y install \
        git-flow

# Set default user
USER tatemz

# Setup SSH keys
COPY --chown=tatemz settings/.ssh /home/tatemz/.ssh
RUN ssh-agent && \
    chmod 500 /home/tatemz/.ssh/*

# Install oh-my-zsh
RUN wget https://github.com/robbyrussell/oh-my-zsh/raw/master/tools/install.sh -O - | zsh || true
ADD --chown=tatemz settings/.zshrc /home/tatemz/.zshrc

# Install NVM
RUN wget -qO- https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | sh && \
    export NVM_DIR="$HOME/.nvm" && \
    [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" && \
    nvm install 8

# Configure Git
RUN git config --global user.name "A. Tate Barber" && \
    git config --global user.email "tate.barber@gmail.com"

# Setup working directory
WORKDIR /home/tatemz/Development
VOLUME [ "/home/tatemz/Development" ]

# Set up starting command
CMD ["/bin/zsh"]
